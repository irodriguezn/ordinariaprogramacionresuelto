/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mates;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.StringTokenizer;


/**
 *
 * @author nacho
 */
public class Mates {
   
    private LinkedList<Integer> numeros=new LinkedList<>();
    private String fichero="numeros.csv";
    
    public Mates() {
        FileReader fr=null;
        BufferedReader br=null;
        StringTokenizer st=null;
        try {
            fr = new FileReader(fichero);
            br=new BufferedReader(fr);
            String linea=br.readLine();
            st = new StringTokenizer(linea);
            while (st.hasMoreTokens()) {
                numeros.add(Integer.parseInt(st.nextToken(";")));
            }
            fr.close();
        } catch (FileNotFoundException ex) {
            System.out.println("No se encuentra el archivo");
        } catch (IOException ex) {
            System.out.println("Error de lectura");
        } 
    }
    
    public Mates(int numLinea) {
        FileReader fr=null;
        BufferedReader br=null;
        StringTokenizer st=null;
        String linea="";
        try {
            fr = new FileReader(fichero);
            br=new BufferedReader(fr);
            for (int i=1; i<=numLinea; i++) {
                linea=br.readLine();
            }
            st = new StringTokenizer(linea);
            while (st.hasMoreTokens()) {
                numeros.add(Integer.parseInt(st.nextToken(";")));
            }
            fr.close();
        } catch (FileNotFoundException ex) {
            System.out.println("No se encuentra el archivo");
        } catch (IOException ex) {
            System.out.println("Error de lectura");
        } 
    }
    
    @Override
    public String toString() {
        String resultado="{";
        Iterator it=numeros.iterator();
        while (it.hasNext()) {
            resultado+=it.next()+", ";
        }
        resultado=resultado.substring(0, resultado.length()-2)+"}";
        return resultado;
    }
    
    public double media() {
        double media=0;
        int num=0;
        int numElementos=0;
        Iterator it=numeros.iterator();
        while (it.hasNext()) {
            num=(Integer)it.next();
            media+=num;
            numElementos++;
        }
        media=media/numElementos;
        return media;        
    }
    
    public static LinkedList<Integer> divisores(int num) {
        LinkedList<Integer> divisores=new LinkedList<>();
        for (int i=num/2; i>=1; i--) {
            if (num%i==0) {
                divisores.add(i);
            }
        }
        return divisores;
    }
    
    public static double[] ecuacionGrado2(int a, int b, int c) throws RuntimeException {
        double raices[]={0,0};
        double raiz, dentroRaiz, numerador, denominador;
        dentroRaiz=Math.pow(b, 2);
        dentroRaiz-=4*a*c;
        if (dentroRaiz<0) {
            throw new ArithmeticException("Raíz Imaginaria");
        }
        raiz=Math.sqrt(dentroRaiz);
        denominador=2*a;
        if (denominador==0) {
            throw new ArithmeticException("División por cero");
        }
        numerador=-b+raiz;
        raices[0]=numerador/denominador;
        numerador=-b-raiz;
        raices[1]=numerador/denominador;
        return raices;
    }
    
    public static double redondear(double num, int decimales) {
        num=num*Math.pow(10, decimales);
        num=Math.round(num);
        num=num/Math.pow(10, decimales);
        return num;
    }   
    
}
