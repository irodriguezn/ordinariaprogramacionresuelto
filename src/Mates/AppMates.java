/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mates;

import java.util.LinkedList;

/**
 *
 * @author nacho
 */
public class AppMates {

    public static void main(String[] args) {
        
        try {
            double [] raices=Mates.ecuacionGrado2(1, -2, 1);
            System.out.println(raices[0]+" y "+ raices[1]);
        } catch (ArithmeticException e) {
            System.out.println(e.getMessage());
        }
        
        Mates m=new Mates();
        System.out.println(m.toString());
        System.out.println(m.media());
        
        
        // Punto extra (Descomentar si se realiza)
        /*m=new Mates(2);
        System.out.println(m.toString());
        System.out.println(m.media());*/
       
        // Salida esperada:
        //{1, 2, 3, 4, 5, 6}
        //3.5
    }
    
}
