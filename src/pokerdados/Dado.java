/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokerdados;

import static pokerdados.Suerte.aleatorio;

/**
 *
 * @author nacho
 */
public class Dado implements Comparable {

    private int valor;
    private String letra;
    
    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
        this.letra=putLetra();
    }

    public String getLetra() {
        return letra;
    }

    public void lanza() {
        valor = aleatorio(6);
        letra = putLetra();
    }

    private String putLetra() {
        String l = "A";
        switch (valor) {
            case 1:
                l = "N";
                break;
            case 2:
                l = "R";
                break;
            case 3:
                l = "J";
                break;
            case 4:
                l = "Q";
                break;
            case 5:
                l = "K";
                break;
            case 6:
                l = "A";
                break;
        }
        return l;
    }

    @Override
    public String toString() {
        return letra;
    }

    @Override
    public int compareTo(Object o) {
        int resultado = 0;
        Dado d = (Dado) o;
        if (d.getValor() > valor) {
            resultado = -1;
        }
        return resultado;
    }
    


}
