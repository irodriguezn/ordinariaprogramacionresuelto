package pokerdados;

import pokerdados.GrpDados;

/**
 *
 * @author nacho
 */
public class Main {

    public static void main(String[] args) {
        GrpDados jugada=new GrpDados(5);
        int tiradas=100000;
        int jugadas[]={0,0,0,0,0,0,0,0};
        for (int i=1; i<=tiradas; i++) {
            jugada.tira();
            jugadas[jugada.getJugada()]++;
        }        
        System.out.println("Nada     : " + (double)jugadas[0]*100/tiradas);
        System.out.println("Pareja   : " + (double)jugadas[1]*100/tiradas);
        System.out.println("Dobles   : " + (double)jugadas[2]*100/tiradas);
        System.out.println("Trío     : " + (double)jugadas[3]*100/tiradas);
        System.out.println("Escalera : " + (double)jugadas[4]*100/tiradas);
        System.out.println("Ful      : " + (double)jugadas[5]*100/tiradas);
        System.out.println("Póquer   : " + (double)jugadas[6]*100/tiradas);
        System.out.println("Repóquer : " + (double)jugadas[7]*100/tiradas);
    }
    
    /* Jugadas Póquer
        jugada= 7 -> Repóquer
        jugada= 6 -> Póquer
        jugada= 5 -> Ful
        jugada= 4 -> Escalera
        jugada= 3 -> Trío
        jugada= 2 -> Dobles
        jugada= 1 -> Pareja
        jugada= 0 -> Nada
    */
    private static void imprimeJugada(int jugada) {
        String resultado="";
        switch (jugada) {
            case 0:
                resultado="Nada";
                break;
            case 1:
                resultado="Pareja";
                break;
            case 2:
                resultado="Dobles parejas";
                break;
            case 3:
                resultado="Trío";
                break;
            case 4:
                resultado="Escalera";
                break;
            case 5:
                resultado="Full";
                break;
            case 6:
                resultado="Póquer";
                break;
            case 7:
                resultado="Repóquer";
                break;
        }
        System.out.println(resultado);;
    }    
    
}
