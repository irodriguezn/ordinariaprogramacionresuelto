/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metodos;

/**
 *
 * @author nacho
 */
public class Main {
    public static void main(String[] args) {
        
        // Descomentar el método a probar
        
        // matrizPasoN(8,3);
        // eratostenes(100);
        // System.out.println(getLinea('-',11));
        // System.out.println(centraCadena("eva", 11));
        // System.out.println(getLinea('-',11));
        // printTriangulo('*', 6);
        // printTriangulo('-', 4);
        // printTriangulo('#', 10);
        // printTriangulo('x', 2);
    }
    
    static String getLinea(char caracter, int numVeces) {
        String linea="";
        for (int i=1; i<=numVeces;i++) {
            linea+=caracter;
        }
        return linea;
    }
    
    static String centraCadena(String cadena, int longitud) {
        int espaciosTotales=longitud-cadena.length();
        int espaciosIzquierda=espaciosTotales/2+espaciosTotales%2;
        String aux=getLinea(' ',espaciosIzquierda);
        aux=aux+cadena;
        return aux;
    }
    
    static void printTriangulo(char caracter, int altura) {
        // 1, 3, 5, 7
        int numCaracteres=altura*2-1;
        for (int i=1; i<=altura; i++) {
            System.out.println(centraCadena(getLinea(caracter,i*2-1), numCaracteres));
        }
    }
    
    static void eratostenes(int n) {
        int[] vCriba=new int[n+1];
        // En primer lugar relleno con los números del 2 al n+1
        // haciendo que coincida con los índices. Los dos primeros
        // me dan lo mismo.
        for (int i=2; i<=n; i++) {
            vCriba[i]=i;
        }
        boolean hayNoPrimos=true;
        int pos=2; //comienzo por el 2
        while (hayNoPrimos) {
            // Marco todas las casillas múltiplos de pos
            // a partir del primer múltiplo. Por ejemplo si pos
            // es 3 sería a partir del 6. O si es 2 a partir del 4.
            for (int i=2*pos; i<=n; i+=pos) {
                vCriba[i]=1;
            }
            // Ahora paso al siguiente número no marcado con 1
            // desde la posición pos+1
            pos++;
            while (vCriba[pos]==1 && pos<n) {
                pos++;
            }            
            // Ahora compruebo la condición de salida
            // Si el cuadrado de pos es mayor que N ya no seguimos
            if (Math.pow(pos, 2)>n) {
                hayNoPrimos=false;
            }
        }
        // Ahora imprimo todos los primos
        String primos="1";
        for (int i=2; i<=n; i++) {
            if (vCriba[i]!=1) {
                primos+=" "+vCriba[i];
            }
        }
        System.out.println("Primos del 1 al "+n);
        System.out.println("--------------------");
        System.out.println(primos);
    }
    
    static void matrizPasoN(int lado, int paso) {
        int matriz[][]=new int[lado][lado];
        int contador=0;
        for (int i=0; i<lado; i++) {
            for (int j=0; j<lado; j++) {
                matriz[i][j]=contador%10;
                contador+=paso;
            }
        }
        muestraMatriz(matriz);
    }
    
    static void muestraMatriz(int m[][]) {
        for (int v[]:m) {
            for (int j:v) {
                System.out.print(j+" ");
            }
            System.out.println("");
        }
    }
}
