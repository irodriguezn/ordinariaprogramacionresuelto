
package Transporte;

/**
 *
 * @author nacho
 */
public class AppTransporte {
    public static void main(String[] args) {
        Coche coche1=new Coche(5,0.1,80);
        coche1.acelera(50); //Velocidad 50Km/h
        coche1.avanza(200);
        coche1.acelera(50); //Velocidad 100Km/h
        coche1.avanza(150);
        coche1.acelera(50); //Velocidad 150Km/h
        coche1.avanza(200);
    }
}
