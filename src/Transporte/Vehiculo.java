
package Transporte;

/**
 *
 * @author nacho
 */
public abstract class Vehiculo {
    
    protected int velocidad;     // Velocidad actual Km/h
    protected double consumo;    // Consumo por Km
    protected double incremento; // Incremento de consumo cada Km/h de más
                               // de incremento de velocidad
    protected double deposito;      // Cantidad de gasolina actual 
    
    public Vehiculo(double consumo, double incremento, double deposito) {
        this.velocidad=0;
        this.consumo=consumo/100;
        this.incremento=incremento/100;
        this.deposito=deposito;
    }
    
    public void acelera(int incrementoKmH) {
        velocidad+=incrementoKmH;
    }
      
    public void decelera(int decrementoKmH) {
        velocidad-=decrementoKmH;
        if (velocidad<0) {velocidad=0;}
    }
    
    public abstract void avanza(int km);
    
}


