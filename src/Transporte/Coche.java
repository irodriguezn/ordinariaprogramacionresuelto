
package Transporte;

/**
 *
 * @author nacho
 */
public class Coche extends Vehiculo {

    public Coche(double consumo, double incremento, double deposito) {
        super(consumo, incremento, deposito);
    }
    
    @Override
    public void avanza(int km) {
        double consumoActual, gastoTotal, kmAvance=0;
        if (deposito>0) {
            // Tenemos que ver si a la velocidad actual tenemos combustible
            // suficiente para avanzar los km pasados
            consumoActual=getConsumoActual();
            gastoTotal=consumoActual*km;
            if (gastoTotal>deposito) {
                // Calculamos ahora cuántos km podemos hacer con el combustible
                // que tenemos a ese consumo actual.
                kmAvance=deposito/consumoActual;
                deposito=0;
            } else {
                kmAvance=km;
                deposito-=gastoTotal;
            }
        }
        System.out.println("Kilometros avanzados: "+ kmAvance);
        System.out.println("Combustible restante: " + deposito);
        if (deposito==0) {
            System.out.println("El coche no tiene combustible para seguir");
        }
    }
    
    private double getConsumoActual() {
        return consumo+velocidad*incremento;
    }
}
